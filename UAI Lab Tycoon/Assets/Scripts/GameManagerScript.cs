using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManagerScript : MonoBehaviour
{
    [Header("Ingresos")]
    [SerializeField]
    int jugadorDinero;
    int gananciaBase = 5;
    SlotPCScript slotPCScript;
    
    [Header("Interfaz de compra/Dinero")]
    [SerializeField] TMPro.TMP_Text dinero;
    [SerializeField] GameObject menuCompra;
    [SerializeField] GameObject menuSetPc;
    [SerializeField] GameObject menuSetExtras;
    [SerializeField] GameObject menuSetMonitor;
    [SerializeField] GameObject infoPclvl1;
    [SerializeField] GameObject infoPclvl2;
    [SerializeField] GameObject infoPclvl3;
    [SerializeField] GameObject infoMonitorlvl1;
    [SerializeField] GameObject infoMonitorlvl2;
    [SerializeField] GameObject infoMonitorlvl3;
    public bool estaEnElMenu;

    [Header("Compra")]
    [SerializeField] int precioProducto;
    [SerializeField] public string productoaComprar;

    [Header("Inventaio")]
    [SerializeField] GameObject inventario;
    [SerializeField] Sprite spritepclvl1;
    [SerializeField] Sprite spritepclvl2;
    [SerializeField] Sprite spritepclvl3;
    [SerializeField] Sprite spritemonitorlvl1;
    [SerializeField] Sprite spritemonitorlvl2;
    [SerializeField] Sprite spritemonitorlvl3;
    public bool tengoalgo;

    [Header("Ganancias")]
    [SerializeField] int ganancias_PClv1 = 0;
    [SerializeField] int cant_PClv1;
    [SerializeField] int ganancias_PClv2 = 0;
    [SerializeField] int cant_PClv2;
    [SerializeField] int ganancias_PClv3 = 0;
    [SerializeField] int cant_PClv3;
    [SerializeField] int ganancias_Monitoreslv1 = 0;
    [SerializeField] int cant_Monitoreslv1;
    [SerializeField] int ganancias_Monitoreslv2 = 0;
    [SerializeField] int cant_Monitoreslv2;
    [SerializeField] int ganancias_Monitoreslv3 = 0;
    [SerializeField] int cant_Monitoreslv3;

    [Header("Contador")]
    [SerializeField] float tiempoRestante;
    [SerializeField] bool timerOn = false;

    void Start()
    {
        tengoalgo = false;
        timerOn = true;
        estaEnElMenu = false;
        menuCompra.SetActive(false);
        inventario.SetActive(false);
        slotPCScript = GameObject.Find("Jugador").GetComponent<SlotPCScript>();
    }

    void Update()
    {
        Contador();

        if (Input.GetKeyDown(KeyCode.B))
        {
            if(estaEnElMenu)
            {
                OcultarMenuCompra();
            }
            else
            {
                MostrarMenuCompra();
            }
        }
        if(Input.GetKeyDown(KeyCode.Z))
        {
            jugadorDinero += 150;
            dinero.text = "$ " + jugadorDinero.ToString();
        }
    }



    private void Contador() 
    {
        if (timerOn)
        {
            if (tiempoRestante > 0)
            {
                tiempoRestante -= Time.deltaTime;
            }
            else
            {
                jugadorDinero += gananciaBase + Ganancias();
                tiempoRestante = 20;
                dinero.text = "$ " + jugadorDinero.ToString();
                timerOn = true;
            }
        }
    }

    private void MostrarMenuCompra()
    {
        menuCompra.SetActive(true);
        menuSetPc.SetActive(false);
        menuSetMonitor.SetActive(false);
        menuSetExtras.SetActive(false);
        infoPclvl1.SetActive(false);
        infoPclvl2.SetActive(false);
        infoPclvl3.SetActive(false);
        infoMonitorlvl1.SetActive(false);
        infoMonitorlvl2.SetActive(false);    
        infoMonitorlvl3.SetActive(false);
        estaEnElMenu = true;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    private void OcultarMenuCompra()
    {
        menuCompra.SetActive(false);
        estaEnElMenu = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
    public void SeleccionProducto(string seleccion)
    {
        switch (seleccion)
        {
            case "pclvl1":
                productoaComprar = "pclvl1";
                precioProducto = 5;
                break;
            case "pclvl2":
                productoaComprar = "pclvl2";
                precioProducto = 10;
                break;
            case "pclvl3":
                productoaComprar = "pclvl3";
                precioProducto = 15;
                break;
            case "monitorlvl1":
                productoaComprar = "monitorlvl1";
                precioProducto = 5;
                break;
            case "monitorlvl2":
                productoaComprar = "monitorlvl2";
                precioProducto = 10;
                break;
            case "monitorlvl3":
                productoaComprar = "monitorlvl3";
                precioProducto = 15;
                break;
            default: 
                break;
        }
    }
    public void Compra()
    {
        if(jugadorDinero >= precioProducto)
        {
            jugadorDinero -= precioProducto;
            dinero.text = "$ " + jugadorDinero.ToString();
            OcultarMenuCompra();
            ActualizarInventario();
            inventario.SetActive(true);
            tengoalgo = true;
            
        }
        else
        {
            Debug.Log("No tenes plata");
        }
        
    }
    private void ActualizarInventario()
    {
        switch (productoaComprar)
        {
            case "pclvl1":
                inventario.GetComponent<UnityEngine.UI.Image>().sprite = spritepclvl1;
                cant_PClv1++;
                break;
            case "pclvl2":
                inventario.GetComponent<UnityEngine.UI.Image>().sprite = spritepclvl2;
                cant_PClv2++;
                break;
            case "pclvl3":
                inventario.GetComponent<UnityEngine.UI.Image>().sprite = spritepclvl3;
                cant_PClv3++;
                break;
            case "monitorlvl1":
                inventario.GetComponent<UnityEngine.UI.Image>().sprite = spritemonitorlvl1;
                ganancias_Monitoreslv1++;
                break;
            case "monitorlvl2":
                inventario.GetComponent<UnityEngine.UI.Image>().sprite = spritemonitorlvl2;
                ganancias_Monitoreslv2++;
                break;
            case "monitorlvl3":
                inventario.GetComponent<UnityEngine.UI.Image>().sprite = spritemonitorlvl3;
                ganancias_Monitoreslv3++;
                break;
            default:
                break;
        }
    }
    public int Ganancias()
    {
        int totalganancias = 0;
        if(cant_PClv1 > 0)
        {
            ganancias_PClv1 = 2 * cant_PClv1;
        }
        if(cant_PClv2> 0)
        {
            ganancias_PClv2 = 3 * cant_PClv2;
        }
        if(cant_PClv3> 0)
        {
            ganancias_PClv3 = 5 * cant_PClv3;
        }
        if (cant_Monitoreslv1 > 0)
        {
            ganancias_Monitoreslv1 = 2 * cant_Monitoreslv1;
        }
        if(cant_Monitoreslv2 > 0)
        {
            ganancias_Monitoreslv2 = 3 * cant_Monitoreslv2;
        }
        if(cant_Monitoreslv3 > 0)
        {
            ganancias_Monitoreslv3 = 5 * cant_Monitoreslv3;
        }

        totalganancias += ganancias_Monitoreslv1 + ganancias_Monitoreslv2 + ganancias_Monitoreslv3 + ganancias_PClv1 + ganancias_PClv2 + ganancias_PClv3;

        return totalganancias;
    }
}
