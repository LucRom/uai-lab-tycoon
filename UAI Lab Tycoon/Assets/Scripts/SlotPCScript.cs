using System.Collections;
using System.Collections.Generic;
using Unity.Burst.CompilerServices;
using UnityEngine;

public class SlotPCScript : MonoBehaviour
{
    [Header("Camara")]
    [SerializeField] Transform camara;

    [Header("Modelos")]
    [SerializeField] GameObject mod_Monitor1;
    [SerializeField] GameObject mod_Monitor2;
    [SerializeField] GameObject mod_Monitor3;
    [SerializeField] GameObject mod_PC1;
    [SerializeField] GameObject mod_PC2;
    [SerializeField] GameObject mod_PC3;
    private GameObject modeloActualPC;
    private GameObject modeloActualMonitor;
    private GameManagerScript modelo;
    RaycastHit hit;

    [Header("Interfaz")]
    [SerializeField] GameObject inventario;
    [SerializeField] GameObject aviso;
    public bool estaEnAviso;

    void Start()
    {
        estaEnAviso = false;
        aviso.SetActive(false);
        modelo = GameObject.Find("GameManager").GetComponent<GameManagerScript>();
    }


    void Update()
    {

        CheckearInventario();

        if (Input.GetMouseButtonDown(0))
        {

            if (Physics.Raycast(camara.position, camara.forward, out hit, 5f))
            {
                if (hit.transform.name == "SlotMonitor" && modelo.tengoalgo)
                {
                    PonerMonitor();
                }
                if (hit.transform.name == "SlotGabinete" && modelo.tengoalgo)
                {
                    PonerPC();
                }
                if (hit.transform.name == "PC LVL1(Clone)" && modelo.tengoalgo && modeloActualPC == mod_PC2)
                {
                    PonerPC();
                }
                if (hit.transform.name == "PC LVL2(Clone)" && modelo.tengoalgo && modeloActualPC == mod_PC3)
                {
                    PonerPC();
                }
                if (hit.transform.name == "Monitor LVL1(Clone)" && modelo.tengoalgo && modeloActualMonitor == mod_Monitor2)
                {
                    PonerMonitor();
                }
                if (hit.transform.name == "Monitor LVL2(Clone)" && modelo.tengoalgo && modeloActualMonitor == mod_Monitor3)
                {
                    PonerMonitor();
                }
                Debug.Log(hit.transform.name);
            }
        }
    }
    private void CheckearInventario()
    {
        if (modelo.tengoalgo)
        {
            switch (modelo.productoaComprar)
            {
                case "pclvl1":
                    modeloActualPC = mod_PC1;
                    break;
                case "pclvl2":
                    modeloActualPC = mod_PC2;
                    break;
                case "pclvl3":
                    modeloActualPC = mod_PC3;
                    break;
                case "monitorlvl1":
                    modeloActualMonitor = mod_Monitor1;
                    break;
                case "monitorlvl2":
                    modeloActualMonitor = mod_Monitor2;
                    break;
                case "monitorlvl3":
                    modeloActualMonitor = mod_Monitor3;
                    break;
                default:
                    break;
            }
        }
    }
    private void PonerPC()
    {
        if (modeloActualPC != null)
        {
            Vector3 posgabinete = hit.transform.position;
            GameObject modeloaReemplazar = Instantiate(modeloActualPC, posgabinete, hit.transform.rotation);
            Destroy(hit.transform.gameObject);
            modeloActualPC = null;
            modelo.tengoalgo = false;
            inventario.SetActive(false);
        }
        else
        {
            MostrarAviso();
        }
    }
    private void PonerMonitor()
    {
        if (modeloActualMonitor != null)
        {
            Vector3 posmonitor = new Vector3(hit.transform.position.x, 1.65f, hit.transform.position.z);
            GameObject modeloaReemplazar = Instantiate(modeloActualMonitor, posmonitor, hit.transform.rotation);
            Destroy(hit.transform.gameObject);
            modeloActualMonitor = null;
            modelo.tengoalgo = false;
            inventario.SetActive(false);
        }
        else
        {
            MostrarAviso();
        }
    }
    private void MostrarAviso()
    {
        aviso.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        estaEnAviso = true;
    }
    public void OcultarAviso()
    {
        aviso.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        estaEnAviso =false;
    }

}