using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoJugador : MonoBehaviour
{
    [Header("Movimiento")]
    [SerializeField] float velocidadMovimiento;
    Rigidbody rb;



    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        float movimientoWS = Input.GetAxis("Vertical") * velocidadMovimiento * Time.deltaTime;
        float movimientoAD = Input.GetAxis("Horizontal") * velocidadMovimiento * Time.deltaTime;

        transform.Translate(movimientoAD, 0, movimientoWS);

    }
}
